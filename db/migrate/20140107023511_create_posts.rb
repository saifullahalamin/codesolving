class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :code_lang
      t.text :code
      t.text :body
      t.integer :author_id

      t.timestamps
    end
  end
end
