// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
// require jquery
//= require jquery-git
// require jquery.turbolinks
//= require jquery_ujs
// require_tree ./ch
//= require jquery.syntaxhighlighter
// require turbolinks

// Supports the following languages out of the box: bsh, c, cc, cpp, cs, csh, cyc, cv, htm, html, java, js, m, mxml, perl, pl, pm, py, rb, sh, xhtml, xml, xsl.
// $(document).ready($.SyntaxHighlighter.init())
//$(document).ready(function(){$.SyntaxHighlighter.init()})
$(function() {
	$.SyntaxHighlighter.init()
	vt = $("#post_title").val();
	vb = $("#post_body").val();
	vc = $("#code_input").val();

	// if (!vt == false) $(".media-heading").text(vt);
	// if (!vb == false) $("mbd").text(vb);
	// if (!vc == false) $('#code_preview').html(('<pre class="highlight">'+vc+'</pre>')).syntaxHighlight();

	$("#post_title").keyup(function() {
		vt = $("#post_title").val();
		if (!vt){
			$(".media-heading").text("Tidak ada judul");
		}
		else $(".media-heading").text(vt);
	});

	$("#post_body").keyup(function() {
		vt = $("#post_body").val();
		if (!vt){
			$("mbd").text("Tidak ada keterangan");
		}
		else $("mbd").text(vt);
	});

	$("#code_input").keyup(function() {
		vc = $("#code_input").val();
		if (!vc){
			vc = '# Berikut adalah bahasa yang didukung: BSH, c, cc, c++, cs, csh, Cyc, cv, htm, html, java, js, m, mxml, perl, pl, pm, py, rb, sh, xhtml , xml, xsl.';
			$('#code_preview').html(('<pre class="highlight">'+vc+'</pre>')).syntaxHighlight();
		}
		$('#code_preview').html(('<pre class="highlight">'+vc+'</pre>')).syntaxHighlight();
	});
});


